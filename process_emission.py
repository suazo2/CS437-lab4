import json
import logging
import sys

import greengrasssdk

# Logging
logger = logging.getLogger(__name__)
logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)

# SDK Client
client = greengrasssdk.client("iot-data")

# c02 tracker
max_co2 = {}


def lambda_handler(event, context):
    logger.info("Received message!")
    logger.info(event)

    global max_co2

    if "reset" in event:
        max_co2 = {}
        return

    # TODO1: Get your data
    if "vehicle_CO2" not in event or "device_id" not in event:
        logger.info("No Data!")
        return

    logger.info(event["vehicle_CO2"])

    # TODO2: Calculate max CO2 emission
    co2 = event["vehicle_CO2"]
    device = event["device_id"]
    max_co2[device] = max(max_co2.get(device, 0), co2)

    data = {
        "device_id": device,
        "max_co2": max_co2[device],
        "timestep_time": event["timestep_time"]
    }

    # TODO3: Return the result to device
    client.publish(
        topic=f"emissions/co2/{device}",
        payload=json.dumps("Max Emission: {}".format(max_co2[device])),
    )

    client.publish(
        topic=f"emissions/co2/all",
        payload=json.dumps(data),
    )

    return